package org.lythiumsoftware.launchers;

import org.lythiumsoftware.converter.ArcadeGamelistFilter;

/**
 * Launcher for arcade filter
 * @author benoit
 */
public class ArcadeGamelistFilterLauncher {
	
	public static void main(String[] args) {
//		ArcadeGamelistFilter fbneoFilter = new ArcadeGamelistFilter("/media/benoit/SHARE/batocera/roms/fbneo/");
//		fbneoFilter.execute();
		ArcadeGamelistFilter mameFilter = new ArcadeGamelistFilter("/home/benoit/nas/benoit/Jeux/Retrogaming/roms/mame.0245/");
		mameFilter.execute();
	}
}
