package org.lythiumsoftware.converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.lythiumsoftware.converter.beans.gamelistxml.EsGame;
import org.lythiumsoftware.converter.beans.gamelistxml.Gamelist;

import com.thoughtworks.xstream.XStream;

public class ArcadeGamelistFilter {

	private String romDirectory;
	private String gamelistPath;
	private static XStream xstream = null;
	
	
	//@formatter:off
	private static List<String> OLD_GAMES_TO_KEEP = Arrays.asList(new String[] {"1942",
																				"1943",
																				"after burner",
																				"alex kidd",
																				"altered beast",
																				"arkanoid",
																				"black tiger",
																				"bubble bobble",
																				"centipede",
																				"chase h.q.",
																				"congo bongo",
																				"continental circus",
																				"contra",
																				"darius",
																				"dig dug",
																				"donkey kong",
																				"double dragon",
																				"enduro racer",
																				"fantasy zone",
																				"galaga",
																				"galaxy force",
																				"gauntlet",
																				"ghouls",
																				"goblins",
																				"gradius",
																				"hang on",
																				"hang-on",
																				"hot chase",
																				"kid niki",
																				"le mans",
																				"looping",
																				"lode runner",
																				"marble madness",
																				"mr. do",
																				"newzealand story",
																				"ninja gaiden",
																				"ninja spirit",
																				"ninja-kid",
																				"opa opa",
																				"outrun",
																				"pac-land",
																				"pac man",
																				"pac man",
																				"pac-man",
																				"pac-man",
																				"pac-mania",
																				"pacman",
																				"pacman",
																				"pole position",
																				"power drift",
																				"q*bert",
																				"rastan",
																				"r-type",
																				"rainbow island",
																				"salamander",
																				"shinobi",
																				"space harrier",
																				"special criminal investigation",
																				"spelunker",
																				"splatter house",
																				"spy hunter",
																				"street fighter",
																				"super sprint",
																				"twinbee",
																				"turbo",
																				"vulcan venture",
																				"wardner",
																				"wonder boy",
																				"zaxxon"});
	//@formatter:on
	
	
	/**
	 * Constructor
	 * @param romDirectory rom directory with ending file separator
	 */
	public ArcadeGamelistFilter(String romDirectory) {
		super();
		this.romDirectory = romDirectory;
		this.gamelistPath = romDirectory +"gamelist.xml";
	}

	//@formatter:on
	public void execute() {
		xstream = initXstream();
		try {
			Gamelist gamelist = (Gamelist) xstream.fromXML(new FileInputStream(gamelistPath));
			System.out.println(gamelist.getGames().size() + "games found in gamelist.");
			Integer removedGamesCount = 0;
			List<EsGame> gamesToRemove = new ArrayList<EsGame>();

			removeUnwantedGames(gamelist, removedGamesCount, gamesToRemove);
			saveGamelist(gamelist);
			
//			hideBiosFromGamelist(gamelist, removedGamesCount, gamesToRemove);
//			saveGamelist(gamelist);
			
			cleanMissingFilesFromGamelist(gamelist, removedGamesCount, gamesToRemove);
			saveGamelist(gamelist);
			
			checkDuplicates(gamelist);
			System.out.println("Removed " + removedGamesCount + "games.");
			saveGamelist(gamelist);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static XStream initXstream() {
		XStream xstream = new XStream();
		xstream = new XStream();
		xstream.processAnnotations(Gamelist.class);
		xstream.ignoreUnknownElements();
		return xstream;
	}

	/**
	 * Removes unwanted games
	 * 
	 * @param xstream
	 * @param gamelist
	 * @param count
	 * @param gamesToRemove
	 * @throws FileNotFoundException
	 */
	private void removeUnwantedGames(Gamelist gamelist, Integer count,
			List<EsGame> gamesToRemove) throws FileNotFoundException {
		System.out.println(
				" ==================================== Removing unwanted games ===============================");
		for (EsGame game : gamelist.getGames()) {
			if (shouldBeDeleted(game)) {
				count++;
				File file = getGameFile(game);
				if (file.exists()) {
					file.delete();
					System.out.println(
							"DELETED " + game.getName() + " " + " : " + game.getGenre() + "  " + game.getReleasedate());
				}
				gamesToRemove.add(game);
			}

		}
		if (!gamesToRemove.isEmpty()) {
			for (EsGame gameToRemove : gamesToRemove) {
				gamelist.getGames().remove(gameToRemove);
			}
		}
	}

	private File getGameFile(EsGame game) {
		return new File(romDirectory + game.getPath().replace("./", ""));
	}

	/**
	 * Checks duplicate roms with different names
	 * 
	 * @param gamelist
	 */
	private void checkDuplicates(Gamelist gamelist) {
		Map<String, List<EsGame>> gamesByTitle = groupGamesByTitle(gamelist);
		for (Entry<String, List<EsGame>> entry : gamesByTitle.entrySet()) {
			if (entry.getValue().size() > 1) {
				List<EsGame> duplicateGames = entry.getValue();
				askForDuplicatesDeletion(gamelist, duplicateGames);
			}
		}
	}

	/**
	 * group games by title in a hashmap - key : game title - value : all games with
	 * the same title
	 * 
	 * @param gamelist the gamelist to filter
	 * @return
	 */
	private Map<String, List<EsGame>> groupGamesByTitle(Gamelist gamelist) {
		Map<String, List<EsGame>> gamesByTitle = new HashMap<String, List<EsGame>>();
		for (EsGame game : gamelist.getGames()) {
			if (!game.getPath().contains("samples") && !isBios(game)) {
				if (!gamesByTitle.containsKey(game.getName())) {
					List<EsGame> liste = new ArrayList<EsGame>();
					liste.add(game);
					gamesByTitle.put(game.getName(), liste);
				} else {
					gamesByTitle.get(game.getName()).add(game);
				}
			}
		}
		return gamesByTitle;
	}

	/**
	 * Asks in the standard input whitch duplicate to keep
	 * 
	 * @param gamelist       the gamelist to process
	 * @param duplicateGames duplicates of one game
	 */
	private void askForDuplicatesDeletion(Gamelist gamelist, List<EsGame> duplicateGames) {
		System.out.println("=============================================================");
		System.out.println(" Duplicate games found :");
		for (int i = 0; i < duplicateGames.size(); i++) {
			System.out.println("[" + i + "]" + getGameDescription(duplicateGames.get(i)));
		}

		Integer gameIndexToKeep = null;
		boolean validAnswer = false;
		System.out.println("Which rom number to keep ? [0," + (duplicateGames.size() - 1) + "], n : do nothing");
		do {
			Scanner inputScanner = new Scanner(System.in);
			String answer = inputScanner.nextLine();
			if (answer.length() == 1) {
				if (answer.equals("n")) {
					validAnswer = true;
					System.out.println("Your selection : keep everything.");
				} else {
					try {
						Integer answerNumber = Integer.parseInt(answer);
						if (answerNumber >= 0 && answerNumber < duplicateGames.size()) {
							validAnswer = true;
							gameIndexToKeep = answerNumber;
							System.out.println("Your selection : keep " + answerNumber + " "
									+ getGameDescription(duplicateGames.get(0)));
						}
					} catch (Exception e) {
						System.out.println("Invalid selection : " + answer);
					}
				}
			}
		} while (!validAnswer);
		if (gameIndexToKeep != null) {
			for (int i = 0; i < duplicateGames.size(); i++) {
				if (i != gameIndexToKeep) {
					EsGame game = duplicateGames.get(i);
					File file = getGameFile(game);
					if (file.exists()) {
						file.delete();
						System.out.println("Deleted file : "+file.getPath());
					}
					gamelist.getGames().remove(game);
					saveGamelist(gamelist);
					System.out.println("Removed game from gamelist " + getGameDescription(game));
				}
			}
		}
	}

	/**
	 * removes bios from the gamelist
	 * 
	 * @param gamelist
	 * @param count
	 * @param gamesToRemove
	 * @throws FileNotFoundException
	 */
	private void hideBiosFromGamelist(Gamelist gamelist, Integer count,
			List<EsGame> gamesToRemove) throws FileNotFoundException {
		System.out.println(
				" ==================================== Hiding bios from gamelist.xml ===============================");
		for (EsGame game : gamelist.getGames()) {
			if (isBios(game)) {
				File file = getGameFile(game);
				System.out.println("PATH " + file.getPath());
				if (file.exists()) {
					System.out.println("BIOS HIDDEN FROM GAMELIST " + count + " " + game.getGenre() + " "
							+ game.getName() + " " + game.getReleasedate());
				}
				gamesToRemove.add(game);
			}

		}
		if (!gamesToRemove.isEmpty()) {
			for (EsGame gameToRemove : gamesToRemove) {
				gamelist.getGames().remove(gameToRemove);
			}
		}
	}

	/**
	 * cleans missing games from the gamelist
	 * 
	 * @param gamelist
	 * @param count
	 * @param gamesToRemove
	 * @throws FileNotFoundException
	 */
	private void cleanMissingFilesFromGamelist(Gamelist gamelist, Integer count,
			List<EsGame> gamesToRemove) throws FileNotFoundException {
		System.out.println(
				" ==================================== Removing missing files from gamelist.xml ===============================");
		for (EsGame game : gamelist.getGames()) {
			File file = getGameFile(game);
			if (!file.exists()) {
				System.out.println("MISSING" + count + getGameDescription(game));
				gamesToRemove.add(game);
			}
		}
		if (!gamesToRemove.isEmpty()) {
			for (EsGame gameToRemove : gamesToRemove) {
				gamelist.getGames().remove(gameToRemove);
			}
		}
	}

	private void saveGamelist(Gamelist gamelist) {
		System.out.println("updated gamelist saved at " + gamelistPath);
		try {
			xstream.toXML(gamelist, new FileOutputStream(gamelistPath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private boolean shouldBeDeleted(EsGame game) {
		String genre = game.getGenre() == null ? "" : game.getGenre();
		String developer = game.getDeveloper() == null ? "" : game.getDeveloper();
		String desc = game.getDesc() == null ? "" : game.getDesc();
		return (isUnwantedName(game.getName())|| isUnwantedGenre(genre) || isUnwantedDeveloper(developer) || isUnwantedDescription(desc)
				|| isOldGameToRemove(game)) && !isBios(game);
	}

	private boolean isBios(EsGame game) {
		String genre = game.getGenre() == null ? "" : game.getGenre();
		return genre.toLowerCase().contains("bios") || game.getName().toLowerCase().contains("bios");
	}

	private boolean isUnwantedDeveloper(String developer) {
		return developer.contains("bootleg") || developer.contains("Bootleg");
	}

	private boolean isUnwantedDescription(String desc) {
		String descLowercase = desc.toLowerCase();
		//@formatter:off
		return descLowercase.contains("mobile phone") 
				|| descLowercase.contains("adult content")
				|| descLowercase.contains("poker")
				|| descLowercase.contains("casino")
				|| descLowercase.contains("adult game") 
				|| descLowercase.contains("adult-content")
				|| descLowercase.contains("nude") 
				|| descLowercase.contains("gals");
		//@formatter:on
	}

	private boolean isUnwantedName(String name) {
		//@formatter:off
		return name.toLowerCase().contains("gals ") 
				||  name.toLowerCase().contains(" gals")
				||  name.toLowerCase().contains("poker")
				||  name.toLowerCase().contains("casino")
				||  name.toLowerCase().contains("mahjong"); 
		//@formatter:on

	}
	
	private boolean isUnwantedGenre(String genre) {
		//@formatter:off
		return genre.contains("Casino") 
				|| genre.contains("Cards") 
				|| genre.contains("Print Club")
				|| genre.contains("Slot") 
				|| genre.contains("Jukebox") 
				|| genre.contains("Game Console")
				|| genre.contains("Business") 
				|| genre.contains("Clock") 
				|| genre.contains("Mahjong")
				|| genre.contains("Electronic Game") 
				|| genre.contains("Handheld") 
				|| genre.contains("Computer")
				|| genre.contains("Printer") 
				|| genre.contains("Tabletop") 
				|| genre.contains("Quiz")
				|| genre.contains("Mini-Games") 
				|| genre.contains("Document Processor") 
				|| genre.contains("Utilities")
				|| genre.contains("Board Game") 
				|| genre.contains("Calculator") 
				|| genre.contains("Darts")
				|| genre.toLowerCase().contains("electromechanical") 
				|| genre.contains("Typewriter") 
				|| genre.contains("Hanafuda")
				|| genre.contains("Rhythm") 
				|| genre.contains("Telephone") 
				|| genre.contains("Cash Counter")
				|| genre.contains("Smartphone") 
				|| genre.contains("DVD Player")
				|| genre.toLowerCase().contains("playing cards")
				|| genre.toLowerCase().contains("bowling") 
				|| genre.toLowerCase().contains("baseball")
				|| genre.toLowerCase().contains("golf") 
				|| genre.toLowerCase().contains("pool")
				|| genre.toLowerCase().contains("boxing") 
				|| genre.toLowerCase().contains("adult");
		//@formatter:on
	}

	private static boolean isOldGameToRemove(EsGame game) {
		boolean ret = false;
		try {
			if (game.getReleasedate() != null && game.getReleasedate().length()>4) {
				Integer year = Integer.parseInt(game.getReleasedate().substring(0, 4));
				ret = (game.getReleasedate() != null //
						&& !game.getReleasedate().isEmpty()//
						&& year <= 1988)//
						&& !isGameToKeep(game.getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	private static boolean isGameToKeep(String gameTitle) {
		boolean toKeep = false;
		for (String oldGameTitle : OLD_GAMES_TO_KEEP) {
			if (gameTitle.toLowerCase().contains(oldGameTitle)) {
				toKeep = true;
				break;
			}
		}
		return toKeep;
	}

	private static String getGameDescription(EsGame game) {
		String description = "";
		if (game != null) {
			String name = game.getName() == null ? "" : game.getName();
			String genre = game.getGenre() == null ? "" : game.getGenre();
			String date = null;
			try {
				date = (game.getReleasedate() == null || game.getReleasedate().length()<4) ? "" : ("(" + game.getReleasedate().substring(0, 4) + ")");
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			String fileName = game.getPath() == null ? "" : game.getPath();
			description = "NAME : " + name + date + " | FILE: " + fileName + " | GENRE " + genre;
		}
		return description;
	}

}
