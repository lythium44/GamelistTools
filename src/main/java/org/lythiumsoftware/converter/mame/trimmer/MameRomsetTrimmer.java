package org.lythiumsoftware.converter.mame.trimmer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;

import org.lythiumsoftware.converter.beans.dat.DatGame;
import org.lythiumsoftware.converter.beans.dat.DataFile;

import com.thoughtworks.xstream.XStream;

public class MameRomsetTrimmer {
	
	//@formatter:off
	private static List<String> OLD_GAMES_TO_KEEP = Arrays.asList(new String[] {"1942","1943",
																				"after burner",
																				"alex kidd",
																				"altered beast",
																				"arkanoid",
																				"black tiger",
																				"bubble bobble",
																				"centipede",
																				"chase h.q.",
																				"congo bongo",
																				"continental circus",
																				"contra",
																				"darius",
																				"dig dug",
																				"donkey kong",
																				"double dragon",
																				"enduro racer",
																				"fantasy zone",
																				"galaga",
																				"galaxy force",
																				"gauntlet",
																				"ghouls",
																				"goblins",
																				"hang on",
																				"hang-on",
																				"hot chase",
																				"le mans",
																				"looping",
																				"lode runner",
																				"marble madness",
																				"mr. do",
																				"newzealand story",
																				"ninja gaiden",
																				"ninja spirit",
																				"ninja-kid",
																				"outrun",
																				"pac-land",
																				"pac man",
																				"pac man",
																				"pac-man",
																				"pac-man",
																				"pac-mania",
																				"pacman",
																				"pacman",
																				"pole position",
																				"power drift",
																				"q*bert",
																				"r-type",
																				"rainbow island",
																				"shinobi",
																				"space harrier",
																				"special criminal investigation",
																				"spelunker",
																				"splatter house",
																				"spy hunter",
																				"street fighter",
																				"super sprint",
																				"twinbee",
																				"vulcan venture",
																				"wardner",
																				"wonder boy",
																				"zaxxon"});
	
	private static List<String> UNWANTED_TITLES = Arrays.asList(new String[] {"jong shin","jang taku"});
	
	private String mameDatFilePath;
	private String sourceRomPath;
	private String newRomPath;
	
	private XStream xstream;
	
	public MameRomsetTrimmer(String mameDatFilePath,String sourceRomPath,String newRomPath) {
		xstream = new XStream();
		xstream.processAnnotations(DataFile.class);
		xstream.ignoreUnknownElements();
		this.mameDatFilePath=mameDatFilePath;
		this.sourceRomPath=sourceRomPath;
		this.newRomPath=newRomPath;
	}
	
	public void trimRomset() {
		try {
			
			DataFile datafile = (DataFile) xstream.fromXML(new FileInputStream(mameDatFilePath));
			
			int copiedGames=0;
			int copiedBios=0;
			System.out.println(datafile.getGames().size());
			for(DatGame game:datafile.getGames()) {
				File rom = new File(sourceRomPath+game.getName()+".zip");
				boolean isWantedGame = isWantedGame(game, rom);
				if(isWantedGame && rom.exists()) {
					copyGame(rom);
					copiedGames++;
				}
				// TODO BIOS
//				else if("game.getIsbios() && rom.exists()) {
//					copyBios(rom);
//					copiedBios++;
//				}
			}
			System.out.println("copied " + copiedGames + " games and " + copiedBios + " bioses" );
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void copyGame(File rom) {
		try {
			Path newPath = Paths.get(newRomPath, "roms",rom.getName());
			if(!newPath.toFile().exists()) {
				Files.copy(Paths.get(rom.getPath()), newPath, StandardCopyOption.COPY_ATTRIBUTES );
			}
			System.out.println("COPIED GAME" + rom.getPath() + " TO " + newPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void copyBios(File rom) {
		try {
			Path newPath = Paths.get(newRomPath, "bios",rom.getName());
			Files.copy(Paths.get(rom.getPath()), newPath, StandardCopyOption.COPY_ATTRIBUTES);
			System.out.println("COPIED BIOS" + rom.getPath() + " TO " + newPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isWantedGame(DatGame game, File rom) {
		boolean wanted = true;
//		if(game.getCloneof()!= null ) {
//			System.out.println("GAME " + game.getName() + " | CLONE OF :" + game.getCloneof() +  " | path :" + rom.getPath() + " | exists :" + rom.exists());
//			wanted =false;
//			
//		} else
		if("yes".equals(game.getIsmechanical())) {
			System.out.println("GAME " + game.getName() + " | MECHANICAL :" + game.getIsmechanical() +  " | path :" + rom.getPath() + " | exists :" + rom.exists());
			wanted = false;
		}
		Integer year=null;
		try {
			year = Integer.parseInt(game.getYear());
		}
		catch(Exception e) {
			
		}
		if(year!=null && year<=1985 ) {
			System.out.println("GAME " + game.getName() + " | TOO OLD :" + game.getYear() +  " | path :" + rom.getPath() + " | exists :" + rom.exists());
			wanted = false;
		}
		if("yes".equals(game.getIsbios())) {
			wanted = false;
		}
		
		return wanted;
	}

}
