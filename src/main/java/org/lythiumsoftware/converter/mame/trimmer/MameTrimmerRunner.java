package org.lythiumsoftware.converter.mame.trimmer;

public class MameTrimmerRunner {
	public static void main(String[] args) {
		MameRomsetTrimmer trimmer = new MameRomsetTrimmer("/home/benoit/nas/benoit/Jeux/Retrogaming/MAME 0.245.dat", //
				"/home/benoit/nas/benoit/Jeux/Retrogaming/mame.0245.revival/", //
				"/home/benoit/nas/benoit/Jeux/Retrogaming/mame.0245.trimmed/");
		trimmer.trimRomset();
	}
}
