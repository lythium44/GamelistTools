package org.lythiumsoftware.converter.beans.listxml;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("mame")
public class Mame {
	@XStreamImplicit(itemFieldName = "machine")
	private List<MameGame> games;

	public List<MameGame> getGames() {
		return games;
	}

	public void setGames(List<MameGame> games) {
		this.games = games;
	}

}
