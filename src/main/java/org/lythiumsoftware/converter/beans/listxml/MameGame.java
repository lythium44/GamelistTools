package org.lythiumsoftware.converter.beans.listxml;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class MameGame {
	@XStreamAsAttribute
	private String name;

	private String description;

	private String year;

	@XStreamAsAttribute
	private String ismechanical;

	@XStreamAsAttribute
	private String isdevice;

	private String manufacturer;
	@XStreamAsAttribute
	private String cloneof;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCloneof() {
		return cloneof;
	}

	public void setCloneof(String cloneof) {
		this.cloneof = cloneof;
	}

	public String isIsmechanical() {
		return ismechanical;
	}

	public void setIsmechanical(String ismechanical) {
		this.ismechanical = ismechanical;
	}

	public String getIsdevice() {
		return isdevice;
	}

	public void setIsdevice(String isdevice) {
		this.isdevice = isdevice;
	}

	public String getIsmechanical() {
		return ismechanical;
	}

}
