package org.lythiumsoftware.converter.beans.dat;

import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class DatGame {
	@XStreamAsAttribute
	private String name;

	@XStreamAsAttribute
	private String year;

	@XStreamAsAttribute
	private String description;



	@XStreamAsAttribute
	private String cloneof;

	@XStreamAsAttribute
	private String isbios;

	@XStreamAsAttribute
	private String ismechanical;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCloneof() {
		return cloneof;
	}

	public void setCloneof(String cloneof) {
		this.cloneof = cloneof;
	}

	public String getIsbios() {
		return isbios;
	}

	public void setIsbios(String isbios) {
		this.isbios = isbios;
	}

	public String getIsmechanical() {
		return ismechanical;
	}

	public void setIsmechanical(String ismechanical) {
		this.ismechanical = ismechanical;
	}
	

}
