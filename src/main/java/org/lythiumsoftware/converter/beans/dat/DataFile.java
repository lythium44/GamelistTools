package org.lythiumsoftware.converter.beans.dat;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("datafile")
public class DataFile {
	@XStreamImplicit(itemFieldName = "machine")
	private List<DatGame> games;

	public List<DatGame> getGames() {
		return games;
	}

	public void setGames(List<DatGame> games) {
		this.games = games;
	}
	
	
	
}
