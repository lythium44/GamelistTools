package org.lythiumsoftware.converter.beans.gamelistxml;

public class Provider {
	private String system;
	
	private String sofware;
	
	private String database;
	
	private String web;

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getSofware() {
		return sofware;
	}

	public void setSofware(String sofware) {
		this.sofware = sofware;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}
	
	
}
