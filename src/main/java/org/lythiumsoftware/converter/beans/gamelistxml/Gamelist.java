package org.lythiumsoftware.converter.beans.gamelistxml;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("gameList")

public class Gamelist {

	private Provider provider;

	@XStreamImplicit(itemFieldName = "game")
	private List<EsGame> games = new ArrayList<EsGame>();

	public List<EsGame> getGames() {
		return games;
	}

	public void setGames(List<EsGame> games) {
		this.games = games;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

}
