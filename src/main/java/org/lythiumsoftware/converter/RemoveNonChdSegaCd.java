package org.lythiumsoftware.converter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.lythiumsoftware.converter.beans.gamelistxml.EsGame;
import org.lythiumsoftware.converter.beans.gamelistxml.Gamelist;

import com.thoughtworks.xstream.XStream;

public class RemoveNonChdSegaCd {

	private static String segaCdDir = "D:\\Jeux\\Retrogaming\\games\\segacd";

	public static void main(String[] args) {
		File[] listFiles = new File(segaCdDir).listFiles();
		XStream xstream = new XStream();
		xstream = new XStream();
		xstream.processAnnotations(Gamelist.class);
		xstream.ignoreUnknownElements();
		Gamelist gamelist = (Gamelist) xstream.fromXML(new File(segaCdDir + "\\gamelist.xml"));
		List<EsGame> gamesToRemove = new ArrayList<EsGame>();
		for (EsGame game : gamelist.getGames()) {
			System.out.println(game.getPath());
			if (!game.getPath().endsWith("chd")) {
				gamesToRemove.add(game);
			}
		}
		for (EsGame game : gamesToRemove) {
			gamelist.getGames().remove(game);
		}

		PrintWriter writer;
		try {
			writer = new PrintWriter(new File(segaCdDir + "\\gamelistNew.xml"));
			xstream.toXML(gamelist, writer);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
