package org.lythiumsoftware.converter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import org.lythiumsoftware.converter.beans.gamelistxml.EsGame;
import org.lythiumsoftware.converter.beans.gamelistxml.Gamelist;

import com.thoughtworks.xstream.XStream;

public class GamelistGenerator {

	public static void main(String[] args) {
		// xstream init
		XStream xstream = new XStream();
		xstream = new XStream();
		xstream.processAnnotations(Gamelist.class);
		xstream.ignoreUnknownElements();
		
		generateGamelist(xstream,"/media/benoit/SHARE/batocera/roms/megadrive/");
		generateGamelist(xstream,"/media/benoit/SHARE/batocera/roms/snes/");
		generateGamelist(xstream,"/media/benoit/SHARE/batocera/roms/nes/");
		generateGamelist(xstream,"/media/benoit/SHARE/batocera/roms/gba/");
		generateGamelist(xstream,"/media/benoit/SHARE/batocera/roms/pcengine/");
	}

	private static void generateGamelist(XStream xstream,String romPath) {
		System.out.println("Generating gamelist for " + romPath);
		int count = 0;
		Gamelist gamelist = new Gamelist();
		File romDirectory = new File(romPath);
		if (romDirectory.exists() && romDirectory.isDirectory()) {
			String[] fileNames = romDirectory.list();
			for (String fileName : fileNames) {
				if (fileName.endsWith(".zip")) {
					String gameName = fileName.replace(".zip", "");
					EsGame game = new EsGame();
					game.setPath("./"+fileName);
					game.setImage("./mixart/" + gameName + ".png");
					game.setMarquee("./wheel/" + gameName + ".png");
					game.setName(gameName);
					gamelist.getGames().add(game);
					count++;
				}
			}
		}
		try {
			xstream.toXML(gamelist, new FileOutputStream(romPath + "gamelist.xml"));
			System.out.println("Wrote gamelist for " + count + " games.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
