package org.lythiumsoftware.converter;

public class EsParameters {
	private String sourceFullPath = "D:\\Jeux\\Retrogaming\\emulators\\Mame201\\listxml.xml";
	private String destinationFullPath = "D:\\gamelist.xml";
	private String romPath = "D:\\Jeux\\Retrogaming\\games\\arcade-mame201";
	private String snapRelativeToRomDirectory = "images";

	public String getSourceFullPath() {
		return sourceFullPath;
	}

	public void setSourceFullPath(String sourceFullPath) {
		this.sourceFullPath = sourceFullPath;
	}

	public String getDestinationFullPath() {
		return destinationFullPath;
	}

	public void setDestinationFullPath(String destinationFullPath) {
		this.destinationFullPath = destinationFullPath;
	}

	public String getRomPath() {
		return romPath;
	}

	public void setRomPath(String romPath) {
		this.romPath = romPath;
	}

	public String getSnapRelativeToRomDirectory() {
		return snapRelativeToRomDirectory;
	}

	public void setSnapRelativeToRomDirectory(String snapRelativeToRomDirectory) {
		this.snapRelativeToRomDirectory = snapRelativeToRomDirectory;
	}

}
