package org.lythiumsoftware.converter;

import java.io.File;

public class CopyMameSnapsToFba {

	private static String fbaSnapsDirectory = "D:\\Jeux\\Retrogaming\\games\\arcade-fba\\images";
	private static File fbaRomsDirectory = new File("D:\\Jeux\\Retrogaming\\games\\arcade-fba");
	private static File mameSnapsDirectory = new File("D:\\Jeux\\Retrogaming\\games\\arcade-mame201\\images");
	private static String mameRomsDirectory = "D:\\Jeux\\Retrogaming\\games\\arcade-mame201";

	public static void main(String args[]) {

		File[] listFiles = fbaRomsDirectory.listFiles();
		for (File fbaRom : listFiles) {
			String imageName = fbaRom.getName().replaceAll(".7z", "");
			File fbaImage = new File(fbaSnapsDirectory + "\\" + imageName + "-image.png");
			if (!fbaImage.exists()) {
				System.out.println(fbaImage.getAbsolutePath());
				File mameImage = new File(mameSnapsDirectory + "\\" + imageName + ".png");
				if (mameImage.exists()) {
					System.out.println("Mame image found : " + mameImage.getAbsolutePath());
				}
			}

		}
	}
}
