//package org.lythiumsoftware.converter;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.PrintWriter;
//
//import org.lythiumsoftware.converter.beans.gamelistxml.Gamelist;
//import org.lythiumsoftware.converter.beans.listxml.Mame;
//import org.lythiumsoftware.converter.beans.listxml.MameGame;
//
//import com.thoughtworks.xstream.XStream;
//
//public class EmulationStationConverter {
//
//	private XStream xstream;
//	private Mame listXml;
//	private Gamelist gamelist;
//	private int writtenGames;
//	private EsParameters params;
//
//	public void convert(EsParameters params) throws FileNotFoundException {
//		this.params = params;
//		initXstreamListXmlReader();
//
//		System.out.println("----------- reading listxml ----------- ");
//		readListXml(new FileInputStream(new File(params.getSourceFullPath())));
//		System.out.println("----------- browsing games ----------- ");
//
//		for (MameGame game : listXml.getGames()) {
//			if (game.get}
//			}else
//
//	{
////				System.out.println(game.getName() + " " + game.isIsmechanical());
//	}
//	}System.out.println("----------- finished ----------- ");
//
////		initXstreamGamelistXmlWriter();
//
////		System.out.println("----------- converting entries to emulationstation format ----------- ");
////		generateGamelistXml();
////
////		System.out.println("-----------  writing to output file ----------- ");
////		writeGamelistXml(params.getDestinationFullPath());
////		System.out.println("-----------  complete ----------- ");
//	}
//
//	private void writeGamelistXml(String destinationFilePath) throws FileNotFoundException {
//		PrintWriter out = new PrintWriter(destinationFilePath);
//		xstream.toXML(gamelist, out);
//	}
//
////	private void generateGamelistXml() {
////		gamelist = new Gamelist();
////
////		Provider provider = new Provider();
////		provider.setSystem("Mame");
////		provider.setSofware("Skraper");
////		provider.setDatabase("ScreenScraper.fr");
////		provider.setWeb("http://www.screenscraper.fr");
////
////		gamelist.setProvider(provider);
////
////		for (MameGame mameGame : listXml.getGames()) {
////			File currentRom = new File(params.getRomPath() + File.separator + mameGame.getName() + ".zip");
////			if (currentRom != null && currentRom.exists()) {
////				System.out.println("found game " + mameGame.getName());
////				EmulationStationGame esGame = new EmulationStationGame();
////				esGame.setName(mameGame.getDescription().replaceAll(" \\(.*\\)", "").trim());
////				esGame.setReleasedate(mameGame.getYear() + "0101T000000");
////				esGame.setPublisher(mameGame.getManufacturer());
////				esGame.setDeveloper(mameGame.getManufacturer());
////				File image = new File(params.getRomPath() + File.separator + params.getSnapRelativeToRomDirectory()
////						+ File.separator + mameGame.getName() + File.separator + "0000.png");
////				// image in romname/0000.png
////				if (image.exists()) {
////					esGame.setImage("./snap/" + mameGame.getName() + "/0000.png");
////				} else {
////					// image in romname.png
////					image = new File(params.getRomPath() + File.separator + params.getSnapRelativeToRomDirectory()
////							+ File.separator + mameGame.getName() + ".png");
////					if (image.exists()) {
////						esGame.setImage("./snap/" + mameGame.getName() + ".png");
////					} else if (mameGame.getCloneof() != null) {
////						// clone
////						image = new File(params.getRomPath() + File.separator + params.getSnapRelativeToRomDirectory()
////								+ File.separator + mameGame.getCloneof() + File.separator + "0000.png");
////						if (image.exists()) {
////							esGame.setImage("./snap/" + mameGame.getCloneof() + "/0000.png");
////						} else {
////							image = new File(
////									params.getRomPath() + File.separator + params.getSnapRelativeToRomDirectory()
////											+ File.separator + mameGame.getCloneof() + ".png");
////							if (image.exists()) {
////								esGame.setImage("./snap/" + mameGame.getCloneof() + "/0000.png");
////							}
////						}
////					}
////				}
////
////				esGame.setPath("./" + mameGame.getName() + ".zip");
////				gamelist.getGames().add(esGame);
////				writtenGames++;
////				System.out.println(esGame.getName());
////				if (writtenGames % 20 == 0) {
////					System.out.println(writtenGames + " games converted ");
////				}
////			}
////		}
////	}
//
//	private void readListXml(FileInputStream xmlSourceInputStream) {
//		listXml = (Mame) xstream.fromXML(xmlSourceInputStream);
//	}
//
//	private XStream initXstreamListXmlReader() {
//		xstream = new XStream();
//		xstream.processAnnotations(Mame.class);
//		xstream.ignoreUnknownElements();
//		return xstream;
//	}
//
////	private XStream initXstreamGamelistXmlWriter() {
////		xstream = new XStream();
////		xstream.processAnnotations(Gamelist.class);
////		xstream.ignoreUnknownElements();
////		return xstream;
////	}
//}
