package org.lythiumsoftware.mame_dat_filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.lythiumsoftware.converter.beans.dat.DatGame;
import org.lythiumsoftware.converter.beans.dat.DataFile;

import com.thoughtworks.xstream.XStream;

public class MameDatFilter {

		private XStream xstream;
		
		private String romSourcePath;
		private String romDestinationPath;
		private String datPath;
		
		public MameDatFilter(String romSourcePath,String romDestinationPath,String datPath) {
			xstream = new XStream();
			xstream.processAnnotations(DataFile.class);
			xstream.ignoreUnknownElements();
			this.romDestinationPath=romDestinationPath;
			this.romSourcePath=romSourcePath;
			this.datPath=datPath;
		}
		
		public void trimRomset() {
			try {
				DataFile datafile = (DataFile) xstream.fromXML(new FileInputStream(datPath));
				System.out.println("FOUND " + datafile.getGames().size() + " GAMES");
				for(DatGame game:datafile.getGames()) {
					processEntry(game);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		private void processEntry(DatGame game) {
			boolean keep = isToKeep(game);
			if(keep) {
//				if(game.getIsbios()) {
//					
//				}
//				else {
//					
//				}
			}
		}

		private boolean isToKeep(DatGame game) {
			boolean keep=true;
			String year = game.getYear();
			Integer yearInt = null;
			try {
				yearInt= Integer.parseInt(year);
			}
			catch (Exception e) {
				
			}
			if(yearInt<1980) {
				keep=false;
			}
			String ismechanical = game.getIsmechanical();
			if(keep & "yes".equals(ismechanical)) {
				keep = false;
			}
			return keep;
		}
}
