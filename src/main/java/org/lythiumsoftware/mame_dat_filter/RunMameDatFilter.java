package org.lythiumsoftware.mame_dat_filter;

public class RunMameDatFilter {
	public static void main(String[] args) {
		String romSourcePath = "/home/benoit/nas/benoit/Jeux/Retrogaming/mame.0245.revival/";
		String romDestinationPath="/home/benoit/nas/benoit/Jeux/Retrogaming/mame.0245.trimmed/";
		String datFilePath="/home/benoit/nas/benoit/Download/DATs/MAME 0.245.dat";
		
		new MameDatFilter(romSourcePath, romDestinationPath, datFilePath).trimRomset();
	}
}
